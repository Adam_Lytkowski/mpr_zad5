package pl.edu.pjatk.mpr.lab5.service;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pl.edu.pjatk.mpr.lab5.model.Address;
import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;
import pl.edu.pjatk.mpr.lab5.model.OrderItem;

public class OrdersServiceTest {
public static void main(String[] args) {
		
		ClientDetails clientSara = new ClientDetails(1, "sada1", "Sara", "G�rska", 21);
		ClientDetails clientMaciej = new ClientDetails(2, "mac12", "Maciej", "Dobrowolski", 25);
		ClientDetails clientPola = new ClientDetails(3, "polandia111", "Pola", "Ostrowska", 10);
		ClientDetails clientKuba = new ClientDetails(4, "jakubson", "Jakub", "Jankowski", 31);
				
		Address addressGdansk = new Address(1, "Rybo�owcow", "4", "55", "80-123", "Gda�sk", "Poland");
		Address addressKrakow = new Address(2, "Walowa", "19", "5", "81-321", "Krak�w", "Poland");
		Address addressElblag = new Address(3, "Czarna", "30", "15", "55-666", "Elbl�g", "Poland");
		Address addressCzestochowa = new Address(4, "Radosna", "6", "666", "12-326", "Czestochowa", "Poland");

		List<OrderItem> orders1 = new ArrayList<OrderItem>();
		List<OrderItem> orders2 = new ArrayList<OrderItem>();
		List<OrderItem> orders3 = new ArrayList<OrderItem>();
		List<OrderItem> orders4 = new ArrayList<OrderItem>();
		List<OrderItem> orders5 = new ArrayList<OrderItem>();
		List<OrderItem> orders6 = new ArrayList<OrderItem>();
		
		OrderItem orderItem1 = new OrderItem(1, "Lays Strong", "ostre czipsy", 3.69);
		OrderItem orderItem2 = new OrderItem(2, "Kabanosy Tarczynski", "Najlepsze kabanosy w Polsce", 10.50);
		OrderItem orderItem3 = new OrderItem(3, "Sony Xperia Z5", "Ostatni telefon SONY z serii Z", 1800.00);
		OrderItem orderItem4 = new OrderItem(4, "Woda Nestle", "Dobra woda za niska cene", 1.20);
		OrderItem orderItem5 = new OrderItem(5, "Volkswagen GOLF IV", "4 wersja z serii Golf", 10000.00);
		OrderItem orderItem6 = new OrderItem(6, "FIFA 17", "Najnowsza czesc gry FIFA", 129.00);
		OrderItem orderItem7 = new OrderItem(7, "Telewizor Samsung", "Telewizor LCD o przekatnej ekranu 42", 1800.00);
		OrderItem orderItem8 = new OrderItem(8, "Laptop Acer", "i5, 8GB RAM, 1TB dysk", 2900.99);
		OrderItem orderItem9 = new OrderItem(9, "Olej silnikowy", "Najlepszy olej dla twojego samochodu", 35.00);
		OrderItem orderItem10 = new OrderItem(10, "Buty Nike", "Najnowsze buty zimowe", 829.00);
		
		orders1 = Arrays.asList(orderItem1, orderItem2, orderItem3, orderItem4, orderItem5, orderItem6);
		orders2 = Arrays.asList(orderItem4, orderItem5, orderItem6, orderItem7, orderItem8, orderItem9);
		orders3 = Arrays.asList(orderItem1, orderItem9, orderItem2, orderItem3, orderItem4);
		orders4 = Arrays.asList(orderItem7, orderItem6, orderItem5, orderItem4);
		orders5 = Arrays.asList(orderItem1, orderItem2, orderItem3, orderItem4);
		orders6 = Arrays.asList(orderItem4, orderItem5, orderItem6, orderItem7);
		

		
		Order order1 = new Order(1,clientPola , addressGdansk, orders1, "Zam�wienie nr 1, bardzo duze zamowienie");
		Order order2 = new Order(2, clientSara, addressElblag, orders2, "AZam�wienie nr 2, srednie zamowienie");
		Order order3 = new Order(3, clientMaciej, addressCzestochowa, orders3, "AZam�wienie nr 3");
		Order order4 = new Order(4, clientKuba, addressKrakow, orders4, "Zam�wienie nr 4");
		Order order5 = new Order(5, clientMaciej, addressGdansk, orders5, "Zam�wienie nr 5");
		Order order6 = new Order(6, clientKuba, addressGdansk, orders6, "Zam�wienie nr 6");
		
		List<Order> orders = new ArrayList<Order>();
		
		orders = Arrays.asList(order1, order2, order3, order4);
		
		System.out.println(OrdersService.findOrdersWhichHaveMoreThan5OrderItems(orders).toString());
		
		System.out.println(OrdersService.findOldestClientAmongThoseWhoMadeOrders(orders).toString());
		
		System.out.println(OrdersService.findOrderWithLongestComments(orders).toString());
		
		System.out.println(OrdersService.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(orders));
		
		System.out.println(OrdersService.getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(orders));
		
		OrdersService.printCapitalizedClientsLoginsWhoHasNameStartingWithS(orders);
		
		System.out.println(OrdersService.groupOrdersByClient(orders).get(clientPola));
		
		System.out.println(OrdersService.partitionClientsByUnderAndOver18(orders).get(true));
		
		}
}
