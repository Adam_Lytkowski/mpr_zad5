package pl.edu.pjatk.mpr.lab5.service;

import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class OrdersService {

    public static List<Order> findOrdersWhichHaveMoreThan5OrderItems(List<Order> orders) {
    	List<Order> moreThan5Stream = orders.stream()
    			.filter(order -> order.getItems().size() > 5)
    			.collect(Collectors.toList());
    	return moreThan5Stream;
    }

    public static ClientDetails findOldestClientAmongThoseWhoMadeOrders(List<Order> orders) {
    	ClientDetails oldestClient = orders.stream()
    			.map(order -> order.getClientDetails())
    			.max(Comparator.comparing(ClientDetails::getAge))
    			.get();
    	
    return oldestClient;
    }

    public static Order findOrderWithLongestComments(List<Order> orders) {
    	Order orderWithLongestComment = orders.stream()
    			.max((n1,n2) -> n1.getComments().length() - n2.getComments().length())
    			.get();
    			
    	return orderWithLongestComment;
    			
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(List<Order> orders) {
    	String getAdults = orders.stream()
    			.filter(order -> order.getClientDetails().getAge() > 18)
    			.map(order -> order.getClientDetails().getName() + " " + order.getClientDetails().getSurname())
    			.distinct()
    			.collect(Collectors.joining(", "));
    	return getAdults;
    }

    public static List<String> getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(List<Order> orders) {
    	List<String> getSorted = orders.stream()
    			.filter(order -> order.getComments().startsWith("A"))
    			.flatMap(order -> order.getItems().stream())
    			.map(item -> item.getName())
    			.distinct()
    			.sorted()
    			.collect(Collectors.toList());
    	return getSorted;
    }

    public static void printCapitalizedClientsLoginsWhoHasNameStartingWithS(List<Order> orders) {
    	orders.stream()
    	.filter(order -> order.getClientDetails().getName().startsWith("S"))
    	.map(order -> order.getClientDetails().getLogin().toUpperCase())
    	.forEach(System.out::println);
    }

    public static Map<ClientDetails, List<Order>> groupOrdersByClient(List<Order> orders) {
    	Map<ClientDetails, List<Order>> groupedOrders = orders.stream()
    			.collect(Collectors.groupingBy(Order::getClientDetails));
    	return groupedOrders;
    }

    public static Map<Boolean, List<ClientDetails>> partitionClientsByUnderAndOver18(List<Order> orders) {
    	Map<Boolean, List<ClientDetails>> clientsUnderAndOver18 = orders.stream()
				.map(order -> order.getClientDetails())
				.distinct()
				.collect(Collectors.partitioningBy(clientDetails -> clientDetails.getAge() > 17));

		return clientsUnderAndOver18;  
    }

}
